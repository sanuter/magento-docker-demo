FROM debian:wheezy
MAINTAINER Steven Trescinski <steven.trescinski@phpro.be>

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
        apt-get install -y curl vim && \
        apt-get install -y apache2 && \
        a2enmod proxy deflate headers expires rewrite && \
        apt-get install -y mysql-server mysql-client && \
        apt-get install -y php5 php5-mysql libapache2-mod-php5 php5-gd php5-cli php5-curl php5-mcrypt php-soap

RUN curl -o /usr/local/bin/n98-magerun.phar https://raw.githubusercontent.com/netz98/n98-magerun/master/n98-magerun.phar && chmod +x /usr/local/bin/n98-magerun.phar
ADD .n98-magerun.yaml /root/

RUN rm -rf /var/www/*
ADD default /etc/apache2/sites-available/

ENV MAGENTO_VERSION_BY_NAME="local-magento-ce-1.9.1.1"

RUN service mysql start && n98-magerun.phar install --dbHost="localhost" --dbUser="root" --dbPass="" --dbName="magentodb" --installSampleData=yes --useDefaultConfigParams=yes --magentoVersionByName="$MAGENTO_VERSION_BY_NAME" --installationFolder="/var/www" --baseUrl="http://magento-docker-demo/"

ADD run.sh /

CMD ["/bin/bash", "/run.sh"]
